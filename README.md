# Get Started

```sh
npm install
# or
yarn install
```
## Run Application

```sh
npm run dev
# or
yarn dev
```

## Run Server
```sh
npm run server
# or
yarn server
```

## Eslint configured

[@rocketseat/eslint-config](https://www.npmjs.com/package/@rocketseat/eslint-config?activeTab=readme)

## Stack
- [React](https://react.dev/)
- [TanStack Query ](https://tanstack.com/query/latest/docs/react/overview)
- [Axios](https://axios-http.com/ptbr/docs/intro)
- [JSON Server](https://www.npmjs.com/package/json-server)
- [React Icons](https://react-icons.github.io/react-icons)