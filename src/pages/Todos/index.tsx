import { FormEvent, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { FaRegTrashAlt, FaUpload } from 'react-icons/fa'

import { addTodo, deleteTodo, getTodos, updateTodo } from '../../api/todosApi'
import './styles.css'

type Props = {
  id: number
  userId: number
  title: string
  completed: boolean
}

export function Todos() {
  const [newTodo, setNewTodo] = useState<string>('')

  const queryClient = useQueryClient()

  const {
    data: todos,
    isLoading,
    isError,
    error,
  } = useQuery<Props[], Error>({
    queryKey: ['todos'],
    queryFn: getTodos,
    select: (data) => data.sort((a, b) => b.id - a.id),
  })

  const addTodoMutation = useMutation({
    mutationFn: addTodo,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['todos'] })
    },
  })

  const updateTodoMutation = useMutation({
    mutationFn: updateTodo,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['todos'] })
    },
  })

  const deleteTodoMutation = useMutation({
    mutationFn: deleteTodo,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['todos'] })
    },
  })

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    const newTodoId = todos?.length! + 1

    addTodoMutation.mutate({
      id: newTodoId,
      userId: 1,
      title: newTodo,
      completed: false,
    })
    setNewTodo('')
  }

  return (
    <main>
      <h1>Todo list</h1>

      <form onSubmit={(e) => handleSubmit(e)}>
        <label htmlFor="new-todo">Enter a new todo item</label>
        <div className="new-todo">
          <input
            type="text"
            id="new-todo"
            value={newTodo}
            onChange={(e) => setNewTodo(e.target.value)}
            placeholder="Enter new todo"
            autoComplete="off"
          />
        </div>
        <button className="submit">
          <FaUpload size={22} />
        </button>
      </form>

      {isLoading ? (
        <p>Loading...</p>
      ) : isError ? (
        <p>{error.message}</p>
      ) : (
        todos?.map((todo) => (
          <article key={todo.id}>
            <div className="todo">
              <input
                type="checkbox"
                checked={todo.completed}
                id={todo.id.toString()}
                onChange={() =>
                  updateTodoMutation.mutate({
                    ...todo,
                    completed: !todo.completed,
                  })
                }
              />
              <label htmlFor={todo.id.toString()}>{todo.title}</label>
            </div>
            <button
              className="trash"
              onClick={() => deleteTodoMutation.mutate(todo.id)}
            >
              <FaRegTrashAlt size={22} />
            </button>
          </article>
        ))
      )}
    </main>
  )
}
