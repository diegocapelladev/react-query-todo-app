import axios from 'axios'

type Todo = {
  id?: number
  userId: number
  title: string
  completed: boolean
}

export const todosAPI = axios.create({
  baseURL: 'http://localhost:3333',
})

export const getTodos = async () => {
  const response = await todosAPI.get('/todos')

  return response.data
}

export const addTodo = async ({ userId, title, completed }: Todo) => {
  await todosAPI.post(`/todos`, {
    userId,
    title,
    completed,
  })
}

export const updateTodo = async ({ id, userId, title, completed }: Todo) => {
  return await todosAPI.put(`/todos/${id}`, {
    id,
    userId,
    title,
    completed,
  })
}

export const deleteTodo = async (id: number) => {
  return await todosAPI.delete(`/todos/${id}`)
}
